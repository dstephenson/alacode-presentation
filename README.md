# A La Code Presentation - MEAN stack

## Directory Layout

    client
        app
            css
                app.css
            js
                app.js
                controllers.js
                filters.js
            partials
                dashboard.tpl.html
                edit-tasks.tpl.html
                tasks.tpl.html
            index.html
        .bowerrc
        bower.json
        package.json
        web-server.js
    server
        bin
            www                             --> Web Server that will run the express application
        models
            category.js
            tag.js
            task.js
        public
            images
            javascripts
            stylesheets
                style.css
        routes
            api.js
        views
            errors.hbs
            index.hbs
            layout.hbs
        app.js
        package.json
    .gitignore
    README.md

## Running Application
