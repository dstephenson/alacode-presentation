'use strict';

angular.module('alacode.presentation')
    .controller('DashboardController', ['$scope', '$rootScope', '$state', 'DS', 'PubNub', function($scope, $rootScope, $state, DS, PubNub) {
        $scope.overDue = [];
        $scope.dueToday = [];
        $scope.dueTomorrow = [];

        $scope.overDueLoaded = false;
        $scope.dueTodayLoaded = false;
        $scope.dueTomorrowLoaded = false;

        $scope.chat = '';

        var tasks = {};
        DS.findAll('task')
            .then(function(data) {
                for (var i = 0; i < data.length; i++) {
                    data[i].normalizeTaskDate();  // Calls angular-data resource method in app.js

                    // Need to make sure that we are only comparing day, month, and year
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);

                    var tomorrow = new Date();
                    tomorrow.setDate(tomorrow.getDate() + 1);

                    // Load our collections, overDue, dueToday and dueTomorrow based on date comparisons
                    if (data[i].due_date < today) {
                        $scope.overDue.push(data[i]);
                    } else if (data[i].due_date.getDate() === today.getDate() && data[i].due_date.getMonth() === today.getMonth() && data[i].due_date.getYear() === today.getYear()) {
                        $scope.dueToday.push(data[i]);
                    } else if (data[i].due_date.getDate() === tomorrow.getDate() && data[i].due_date.getMonth() == tomorrow.getMonth() && data[i].due_date.getYear() === tomorrow.getYear()) {
                        $scope.dueTomorrow.push(data[i]);
                    }
                }

                $scope.overDueLoaded = true;
                $scope.dueTodayLoaded = true;
                $scope.dueTomorrowLoaded = true;
            }, function(response) {
                console.log('Cannot retrieve tasks');

                $scope.overDueLoaded = true;
                $scope.dueTodayLoaded = true;
                $scope.dueTomorrowLoaded = true;
            });

        $scope.viewTasks = function() {
            $state.go('tasks.list');
        };

        // Handles our chat logic
        PubNub.ngSubscribe({channel: 'alacode-chat'});
        $rootScope.$on(PubNub.ngMsgEv('alacode-chat'), function(ngEvent, payload) {
            $scope.$apply(function() {
                $scope.chat += payload.message + '\n\n';
                $scope.chatText = '';
            });
        });

        $scope.sendMessage = function(text) {
            PubNub.ngPublish({channel: 'alacode-chat', message: text});
        }
    }])
    .controller('TasksController', ['$scope', 'DS', function ($scope, DS) {
        $scope.categories = {};
        $scope.tags = {};

        $scope.categoriesLoaded = false;
        DS.findAll('category')
            .then(function(data) {
                $scope.categories = data;
                $scope.categoriesLoaded = true;
            }, function(response) {
                console.log('Cannot load categories');
                $scope.categoriesLoaded = true;
            });

        $scope.tagsLoaded = false;
        DS.findAll('tag')
            .then(function(data) {
                $scope.tags = data;
                $scope.tagsLoaded = true;
            }, function(response) {
                console.log('tags were not loaded');

                $scope.tagsLoaded = true;
            });
    }])
    .controller('TasksListController', ['$scope', '$rootScope', '$state', 'DS', 'PubNub', function ($scope, $rootScope, $state, DS, PubNub) {
        $scope.tasks = [];
        $scope.task =  {};
        $scope.deleteButton = false;

        // Look for any new resources created outside of our local instance
        PubNub.ngSubscribe({channel: 'alacode-presentation'});
        $rootScope.$on(PubNub.ngMsgEv('alacode-presentation'), function(ngEvent, payload) {
            $scope.$apply(function() {
                var local = payload.message.local;

                if (!local) {
                    var resource = payload.message.data;
                    resource.id = resource._id;
                    var type = payload.message.type;
                    if (type === 'Task') {
                        $scope.tasks.push(resource);
                        DS.inject('task', resource);
                        toastr.success('Someone else created a new task called ' + resource.name + ' and it was added to your list');
                    }
                }
            });
        });

        $scope.tasksLoaded = false;
        DS.findAll('task')
            .then(function(data) {
                for (var i = 0; i < data.length; i++) {
                    data[i].normalizeTaskDate();
                    $scope.tasks.push(data[i]);
                }
                $scope.tasksLoaded = true;
            }, function(response) {
                console.log('Cannot load tasks');
                $scope.tasksLoaded = true;
            });

        $scope.addTask = function(task) {
            DS.create('task', task)
                .then(function(task) {
                    $scope.tasks.push(task);

                    $scope.task.name = '';
                    $scope.task.description = '';
                    $scope.task.due_date = '';
                    $scope.task.priority = '';
                    $scope.task.category = '';
                    $scope.task.tags = '';

                    toastr.success('You successfully added a new task!');
                });
        };

        $scope.editTask = function(task) {
            $state.go('tasks.edit', {id: task.id});;
        };

        $scope.confirm = function () {
            $scope.deleteButton = true;
        };

        $scope.cancelDelete = function () {
            $scope.deleteButton = false;
        };

        $scope.removeTask = function(task) {
            DS.destroy('task', task.id)
                .then(function(data) {
                    toastr.success('Task was deleted');

                    for (var i = 0; i < $scope.tasks.length; i++) {
                        if ($scope.tasks[i].id == data) {
                            $scope.tasks.splice(i, 1);
                            break;
                        }
                    }

                    $scope.deleteButton = false;
                })
        };
    }])
    .controller('TasksEditController', ['$scope', '$state', '$stateParams', 'DS', function($scope, $state, $stateParams, DS) {
        $scope.task = {};
        var task = DS.get('task', $stateParams.id);
        if (task === undefined) {
            DS.find('task', $stateParams.id)
                .then(function(data) {
                    $scope.task = data;
                }, function(response) {
                    console.log('Failing...' + JSON.stringify(response));
                })
        } else {
            $scope.task = task;
        }

        $scope.save = function(task) {
            DS.update('task', task.id, task)
                .then(function (task) {
                    toastr.success('Task was updated successfully!');
                    $state.go('tasks.list');
                }, function(response) {
                    toastr.error('Task was not updated: ' + response + '!');
                });
        };

        $scope.cancel = function() {
            $state.go('tasks.list');
        };
    }]);
