'use strict';

angular.module("alacode.presentation", [
    'ui.router',
    'ngCookies',
    'angular-data.DSCacheFactory',
    'angular-data.DS',
    'pubnub.angular.service'
])
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', 'DSProvider', function($stateProvider, $urlRouterProvider, $httpProvider, DSProvider) {
        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'partials/dashboard.tpl.html',
                controller: 'DashboardController'
            }).state('tasks', {
                abstract: true,
                url: '/tasks',
                template: '<ui-view/>',
                controller: 'TasksController'
            }).state('tasks.list', {
                url: '',
                templateUrl: 'partials/tasks.tpl.html',
                controller: 'TasksListController'
            }).state('tasks.edit', {
                url: '/:id/edit',
                templateUrl: 'partials/edit-tasks.tpl.html',
                controller: 'TasksEditController'
            });

        $urlRouterProvider.otherwise(function($injector, $location) {
            // to avoid infinite loops this must be handled in a function as such
            // https://www.bountysource.com/issues/1415500-infinite-loop-when-redirecting-to-state-from-statechangeerror-handler

            window.location = '#/dashboard';
        });

        // Sets the default URL used by Angular Data (needs to be changed to a remote URL when this is available)
        DSProvider.defaults.baseUrl = 'http://localhost:3000/api';

        // MongoDB has an _id passed back, Angular Data must have an id
        DSProvider.defaults.deserialize = function(resourceName, res) {
            if (res.data && res.data.length) {
                for (var i = 0; i < res.data.length; i++) {
                    var item = res.data[i];
                    if (item._id) {
                        item.id = item._id;
                    }
                }
            } else {
                res.data.id = res.data._id;
            }

            return res.data;
        };
    }])
    .run(['$rootScope', 'DS', 'PubNub', function ($rootScope, DS, PubNub) {
        // Different events that are fired off on state change
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            console.log('$stateChangeStart: ' + toState.name);
        });

        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
            console.error('$stateChangeError: ' + toState.name);
            console.error('event: ' + event);
            console.log('error: ' + JSON.stringify(error));
        });

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            console.log('$stateChangeSuccess: ' + toState.name);
        });

        // Only initialize PubNub if necessary
        if (!$rootScope.initialized) {
            PubNub.init({
                publish_key: 'pub-c-79f8f383-a7e1-4bfb-b697-044f4ccab656',
                subscribe_key: 'sub-c-07a3f7b0-d30c-11e4-adb1-02ee2ddab7fe'
            });

            $rootScope.initialized = true;
        }

        // Define Angular Data Resources
        DS.defineResource({
            name: 'task',
            endpoint: 'tasks',
            methods: {
                normalizeTaskDate: function() {
                    var taskDate = {};
                    if (typeof this.due_date === 'string') {
                        var dates = this.due_date.split('T')[0].split('-');
                        taskDate = new Date(dates[0], dates[1] - 1, dates[2]);
                    } else {
                        taskDate = this.due_date;
                    }
                    this.due_date = taskDate;
                }
            }
        });

        DS.defineResource({
            name: 'category',
            endpoint: 'categories',
            methods: {
                getCategoryName: function() {
                    return this.name;
                }
            }
        });

        DS.defineResource({
            name: 'tag',
            endpoint: 'tags',
            methods: {
                getTagName: function() {
                    return this.name;
                }
            }
        })
    }]);
