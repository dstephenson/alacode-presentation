/*
Middleware modules
    req -> middleware -> middleware -> middleware -> res

    A middleware module processes a req and can either return a response or pass it to the next middleware module
 */
// Core Node modules, do not have to be included in package.json
var path = require('path');

// Other Node modules included in package.json
var express = require('express');  // load the express module (framework)
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');  // module that allows us to grab items from a POST more easily

var mongoose = require('mongoose');
mongoose.connect('mongodb://test:test@ds031278.mongolab.com:31278/alacode-presentation');

// Routes
var api = require('./routes/api');

// Express application initialization
var app = express();

app.all('*', function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');  // We can receive requests from anywhere (CORS)
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    next();
});

// view engine setup - handlebars (specified in scaffolding with --hbs)
app.set('views', path.join(__dirname, 'views')); // looks for .hbs files inside the 'views' folder
app.set('view engine', 'hbs');

app.use(logger('dev')); // middleware - sets logger level to 'dev'
app.use(bodyParser.json()); // middleware - looks at request body and adds any JSON content as properties on request.body
app.use(bodyParser.urlencoded({ extended: false })); // middleware - handles urlencoded content
app.use(cookieParser()); // middleware - populate request.cookies with any cookies that exist
app.use(express.static(path.join(__dirname, 'public'))); // middleware - serves up files in the public directory (allows you to protect other files)

// searches for a matching route
app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err  // In development, we want to see the entire stack trace
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}  // In production, we do not want to show the end user the entire stack trace
  });
});


module.exports = app;
