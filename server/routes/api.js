var express = require('express');
var router = express.Router();

// PubNub Service
var pubnub = require("pubnub")({
    publish_key: 'pub-c-79f8f383-a7e1-4bfb-b697-044f4ccab656',
    subscribe_key: 'sub-c-07a3f7b0-d30c-11e4-adb1-02ee2ddab7fe'
});

// Models
var Tag = require('../models/tag');
var Category = require('../models/category');
var Task = require('../models/task');

router.get('/', function(req, res, next) {
    res.json({message: 'You\'ve hit the API, congrats, Hello!'})
});

router.route('/tasks')
    .get(function(req, res) {
        Task.find(function(err, tasks) {
            if (err) {
                res.send(err);
            }
            res.json(tasks);
        });
    })
    .post(function(req, res) {
        var task = new Task();
        task.name = req.body.name;
        task.description = req.body.description;
        task.priority = req.body.priority;
        task.due_date = req.body.due_date;
        task.last_modified = Date.now();
        task.categories.push(req.body.category_id);
        task.tags.push(req.body.tag_id);

        task.save(function(err) {
            if (err) {
                res.send(err);
            }
        });

        var message = {data: task, type: 'Task', local: req.headers['host'].indexOf('localhost') !== -1};
        pubnub.publish({
            channel: 'alacode-presentation',
            message: message,
            callback: function(e) { console.log('Successfully published!'); },
            error: function(e) { console.log('Failed', e); }
        });

        res.json(task);
    });

router.route('/tasks/:task_id')
    .get(function(req, res) {
        Task.findById(req.params.task_id, function(err, task) {
            if (err) {
                res.send(err);
            }
            res.json(task);
        });
    })
    .put(function(req, res) {
        Task.findById(req.params.task_id, function(err, task) {
            if (err) {
                res.send(err);
            }

            // Should always send the entire object
            task.name = req.body.name;
            task.description = req.body.description;
            task.priority = req.body.priority;
            task.due_date = req.body.due_date;
            task.last_modified = Date.now();
            task.categories.push(req.body.category_id);
            task.tags.push(req.body.tag_id);

            task.save(function(err) {
                if (err) {
                    res.send(err);
                }
            });

            res.json(task);
        });
    })
    .delete(function(req, res) {
        Task.remove({
            _id: req.params.task_id
        }, function(err, task) {
            if (err) {
                res.send(err);
            }

            res.json({message: 'Task deleted successfully!'});
        });
    });

router.route('/categories')
    .get(function(req, res) {
        Category.find(function(err, categories) {
            if (err) {
                res.send(err);
            }
            res.json(categories);
        });
    })
    .post(function(req, res) {
        var category = new Category();
        category.name = req.body.name;
        category.description = req.body.description;

        category.save(function(err) {
            if (err) {
                res.send(err);
            }
            res.json({message: 'Category created successfully!'});
        })
    });

router.route('/categories/:category_id')
    .get(function(req, res) {
        Category.findById(req.params.category_id, function(err, category) {
            if (err) {
                res.send(err);
            }
            res.json(category);
        });
    })
    .put(function(req, res) {
        Category.findById(req.params.category_id, function(err, category) {
            if (err) {
                res.send(err);
            }

            category.name = req.body.name;
            category.description = req.body.description;

            Category.save(function(err) {
                if (err) {
                    res.send(err);
                }
                res.json({message: 'Category updated successfully!'});
            });
        });
    })
    .delete(function(req, res) {
        Category.remove({
            _id: req.params.category_id
        }, function(err, category) {
            if (err) {
                res.send(err);
            }

            res.json({message: 'Category deleted successfully!'});
        });
    });

router.route('/tags')
    .get(function(req, res) {
        Tag.find(function(err, tags) {
            if (err) {
                res.send(err);
            }
            res.json(tags);
        });
    })
    .post(function(req, res) {
        var tag = new Tag();
        tag.name = req.body.name;

        tag.save(function(err) {
            if (err)
                res.send(err);
            res.json({message: 'Tag saved successfully!'});
        })
    });

router.route('/tags/:tag_id')
    .get(function(req, res) {
        Tag.findById(req.params.tag_id, function(err, tag) {
            if (err) {
                res.send(err);
            }
            res.json(tag);
        });
    })
    .put(function(req, res) {
        Tag.findById(req.params.tag_id, function(err, tag) {
            if (err) {
                res.send(err);
            }

            tag.name = req.body.name;

            tag.save(function(err) {
                if (err) {
                    res.send(err);
                }
                res.json({message: 'Tag updated successfully!'});
            });
        });
    })
    .delete(function(req, res) {
        Tag.remove({
            _id: req.params.tag_id
        }, function(err, tag) {
            if (err) {
                res.send(err);
            }

            res.json({message: 'Tag deleted successfully!'});
        });
    });

module.exports = router;
