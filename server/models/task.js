var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

/*
Data Type Options

String, Number, Date, Buffer, Boolean, Mixed, Objectid, Array
 */

var TaskSchema  = new Schema({
    name: String,
    description: String,
    priority: Number,
    due_date: Date,
    last_modified: Date,
    categories: [
        {type: Schema.Types.ObjectId, ref: 'Category'}
    ],
    tags: [
        {type: Schema.Types.ObjectId, ref: 'Tag'}
    ]
});

module.exports = mongoose.model('Task', TaskSchema);