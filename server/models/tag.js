var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

/*
 Data Type Options

 String, Number, Date, Buffer, Boolean, Mixed, Objectid, Array
 */

var TagSchema  = new Schema({
    name: String
});

module.exports = mongoose.model('Tag', TagSchema);