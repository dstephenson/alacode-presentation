var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

/*
 Data Type Options

 String, Number, Date, Buffer, Boolean, Mixed, Objectid, Array
 */

var CategorySchema  = new Schema({
    name: String,
    description: String
});

module.exports = mongoose.model('Category', CategorySchema);